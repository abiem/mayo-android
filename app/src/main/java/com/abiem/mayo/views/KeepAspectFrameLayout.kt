package com.abiem.mayo.views

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.abiem.mayo.R

class KeepAspectFrameLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : FrameLayout(context, attrs) {
    private val defaultRatio = 1.5f
    private var ratio = defaultRatio

    init {
        attrs?.let {
            CloseableTypedArray(context.obtainStyledAttributes(attrs, R.styleable.KeepAspectFrameLayout)).use {
                ratio = it.ta.getFloat(R.styleable.KeepAspectFrameLayout_ratio, defaultRatio)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(measuredWidth, (measuredWidth * ratio).toInt())
    }
}
