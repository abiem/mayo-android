package com.abiem.mayo.views

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import com.abiem.mayo.R

class KeepAspectImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ImageView(context, attrs) {
    private val defaultRatio = 1.5f
    private var ratio = defaultRatio

    init {
        attrs?.let {
            CloseableTypedArray(context.obtainStyledAttributes(attrs, R.styleable.KeepAspectImageView)).use {
                ratio = it.ta.getFloat(R.styleable.KeepAspectImageView_ratio, defaultRatio)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(measuredWidth, (measuredWidth * ratio).toInt())
    }
}