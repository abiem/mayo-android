package com.abiem.mayo.views

import android.content.res.TypedArray
import java.io.Closeable

class CloseableTypedArray(val ta: TypedArray) : Closeable {
    override fun close() = ta.recycle()
}