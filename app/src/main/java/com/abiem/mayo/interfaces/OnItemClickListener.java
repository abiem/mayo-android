package com.abiem.mayo.interfaces;

import android.view.View;

import com.abiem.mayo.models.Message;

/**
 * Created by Lakshmi on 14/03/18.
 */

public interface OnItemClickListener {
    void onItemClick(View view,int position,boolean isSelected);
}
