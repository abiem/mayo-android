package com.abiem.mayo.firebase.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.abiem.mayo.Utility.CommonUtility;

/**
 * Created by Lakshmikodali on 15/01/18.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        String refreshedToken = FirebaseInstanceId.getInstance().getInstanceId() + "";
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        CommonUtility.setDeviceToken(refreshedToken,getApplicationContext());
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}