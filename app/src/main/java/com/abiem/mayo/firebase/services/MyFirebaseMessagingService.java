package com.abiem.mayo.firebase.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.abiem.mayo.Utility.ClickActions;
import com.abiem.mayo.Utility.CommonUtility;
import com.abiem.mayo.Utility.Constants;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Lakshmikodali on 15/01/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String title = null, body = null;
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        if (notification != null) {
            title = notification.getTitle();
            body = notification.getBody();
        }
        Map<String, String> data = remoteMessage.getData();
        JSONObject dataObject = new JSONObject(data);
        try {
            int notificationType = dataObject.getInt(Constants.Notifications.sNotification_Type);
            switch (notificationType) {
                case Constants.Notifications.sNOTIFICATION_MESSAGE:
                    String senderId = dataObject.getString(Constants.Notifications.sSenderId);
                    if (senderId != null) {
                        String currentUserId = CommonUtility.getUserId(this);
                        if (currentUserId != null && senderId.equals(currentUserId)) {
                            return;
                        } else {
                            String channelId = dataObject.getString(Constants.Notifications.sChannelId);
                            if (title != null && body != null) {
                                setNotification(title, body, ClickActions.ACTION_MESSAGE, channelId);
                            }
                        }
                    }
                    break;
                case Constants.Notifications.sNOTIFICATION_WERE_THANKS:
                    if (title != null && body != null) {
                        setNotificationForOpenMapActivity(title, body, ClickActions.ACTION_MAP);
                    }
                    break;
                case Constants.Notifications.sNOTIFICATION_TOPIC_COMPLETED:
                    if (title != null && body != null) {
                        String taskId = dataObject.getString(Constants.Notifications.sChannelId);
                        setNotificationForOpenMapActivityWithType(title, body, taskId, notificationType, Constants.Notifications.sChannelId);
                    }
                    break;
                case Constants.Notifications.sNOTIFICATION_NEARBY_TASK:
                    if (title != null && body != null) {
                        String taskId = dataObject.getString(Constants.Notifications.sTaskId);
                        setNotificationForOpenMapActivityWithType(title, body, taskId, notificationType, Constants.Notifications.sTaskId);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setNotification(String pMessageBody, String pSubTitle, String pClickAction, String pChannelId) {
        NotificationCompat.Builder builder = CommonUtility.notificationBuilder(this, pMessageBody, pSubTitle);
        Intent notificationIntent = new Intent(pClickAction);
        notificationIntent.putExtra(Constants.Notifications.sChannelId, pChannelId);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0, builder.build());
        }
    }

    private void setNotificationForOpenMapActivity(String pMessageBody, String pSubTitle, String pClickAction) {
        NotificationCompat.Builder builder = CommonUtility.notificationBuilder(this, pMessageBody, pSubTitle);
        Intent notificationIntent = new Intent(pClickAction);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 101, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(1, builder.build());
        }
    }

    private void setNotificationForOpenMapActivityWithType(String pMessageBody, String pSubTitle, String taskId, int notificationType, String key) {
        NotificationCompat.Builder builder = CommonUtility.notificationBuilder(this, pMessageBody, pSubTitle);
        Intent notificationIntent = getMapActivityIntent(taskId, notificationType, key);
        PendingIntent contentIntent = PendingIntent.getActivity(
                this,
                101,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(contentIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(1, builder.build());
        }
    }

    @NonNull
    private Intent getMapActivityIntent(String taskId, int notificationType, String key) {
        Intent notificationIntent = new Intent(ClickActions.ACTION_MAP);
        String typeStr = String.valueOf(notificationType);
        notificationIntent.putExtra(Constants.Notifications.sNotification_Type, typeStr);
        notificationIntent.putExtra(key, taskId);
        return notificationIntent;
    }
}
