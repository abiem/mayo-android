package com.abiem.mayo.firebase.database;

public interface TaskExpireHandler {
    void taskExpireAlert();
}
