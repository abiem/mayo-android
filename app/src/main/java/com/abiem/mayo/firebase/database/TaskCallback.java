package com.abiem.mayo.firebase.database;

import com.abiem.mayo.models.Task;

public interface TaskCallback {
    void onTaskLoaded(Task task);
}
