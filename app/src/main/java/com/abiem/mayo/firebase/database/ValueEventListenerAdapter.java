package com.abiem.mayo.firebase.database;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class ValueEventListenerAdapter implements ValueEventListener {
    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) { }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) { }
}
