package com.abiem.mayo.classes;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.CardView;
import android.view.View;

/**
 * Created by Lakshmi on 19/02/18.
 */

public class AnimateCard {
    private CardView mCardView;
    private AnimatorSet mAnimatorSet;

    public AnimateCard(CardView pCardView) {
        mCardView = pCardView;
    }

    private AnimatorSet fadeInOutAnimation(View pView) {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(pView, "alpha", 1f, .3f);
        fadeOut.setDuration(2000);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(pView, "alpha", .3f, 1f);
        fadeIn.setDuration(2000);
        AnimatorSet animationSet = new AnimatorSet();
        animationSet.play(fadeIn).after(fadeOut);
        return animationSet;
    }

    public void playFadeInOutAnimation() {
        if (mCardView != null) {
            mCardView.setVisibility(View.VISIBLE);
        }
        final AnimatorSet aset = fadeInOutAnimation(mCardView);
        aset.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                aset.start();
            }
        });
        aset.start();
        mAnimatorSet = aset;
    }

    public void finishAnimation() {
        if (mAnimatorSet != null) {
            mAnimatorSet.cancel();
            mAnimatorSet = null;
        }
        mCardView.setVisibility(View.GONE);
    }
}
