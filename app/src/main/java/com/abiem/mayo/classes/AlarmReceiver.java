package com.abiem.mayo.classes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.abiem.mayo.Utility.Constants;

/**
 * Created by Lakshmi on 19/03/18.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.putExtra(Constants.Notifications.sAlarmManagerNotification, Constants.Notifications.sAlarmManager_Cmg);
        if (context != null) {
            context.sendBroadcast(i);
        }
    }
}
