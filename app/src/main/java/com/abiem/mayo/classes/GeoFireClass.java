package com.abiem.mayo.classes;

import android.location.Location;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.DatabaseError;
import com.abiem.mayo.Utility.CommonUtility;
import com.abiem.mayo.Utility.Constants;
import com.abiem.mayo.activities.MapActivity;
import com.abiem.mayo.firebase.database.FirebaseDatabase;


/**
 * Created by Lakshmi on 06/02/18.
 */

public class GeoFireClass {
    private GeoFire mGeoFire, mTaskGeoFire;
    private MapActivity mapActivity;
    private boolean isNewCardMake;

    public GeoFireClass(MapActivity pContext) {
        mapActivity = pContext;
    }

    public void setGeoFire() {
        FirebaseDatabase firebaseDatabase = new FirebaseDatabase(mapActivity);
        mGeoFire = firebaseDatabase.locationUpdatesOfUserLocationWithGeoFire();
        mTaskGeoFire = firebaseDatabase.getTaskLocationWithGeoFire();
    }

    public void sendDataToFirebaseOfUserLocation(Location plocation) {
        mGeoFire.setLocation(CommonUtility.getUserId(mapActivity),
                new GeoLocation(plocation.getLatitude(), plocation.getLongitude()), new GeoFire.CompletionListener() {
                    @Override
                    public void onComplete(String key, DatabaseError error) {
                        mapActivity.setGeoFireCompleteListener();
                    }
                });
    }

    // this method will return user id
    public GeoQuery setGeoQuery(Location pLocation) {
        GeoQuery mGeoQuery = mGeoFire.queryAtLocation(new GeoLocation(pLocation.getLatitude(),
                pLocation.getLongitude()), Constants.sKeyForMapRadiusInDouble);
        mGeoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                mapActivity.getNearByUsers(key, location);
            }

            @Override
            public void onKeyExited(String key) {
                mapActivity.moveMarkerOutsideFromCurrentLocation(key);
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                mapActivity.onGeoQueryError(error);
            }
        });
        return mGeoQuery;
    }

    //this method will return task id
    public GeoQuery setGeoQueryForTaskFetch(Location pLocation) {
        GeoQuery geoQuery = mTaskGeoFire.queryAtLocation(new GeoLocation(pLocation.getLatitude(),
                pLocation.getLongitude()), Constants.sKeyForMapRadiusInDouble);
        mapActivity.removeCardsFromViewPager();
        isNewCardMake = false;
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                mapActivity.getNearByTask(key, location, isNewCardMake);
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                mapActivity.fetchAfterNearByTask();
                isNewCardMake = true;
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
        return geoQuery;
    }
}
