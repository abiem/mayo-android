package com.abiem.mayo.components

import android.app.AlarmManager
import android.app.IntentService
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.AlarmManagerCompat
import android.support.v4.content.LocalBroadcastManager
import androidx.work.*
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import com.abiem.mayo.Notifications.PushNotificationManager
import com.abiem.mayo.R
import com.abiem.mayo.Utility.CommonUtility
import com.abiem.mayo.Utility.Constants
import com.abiem.mayo.Utility.logd
import com.abiem.mayo.activities.MapActivity
import com.abiem.mayo.application.MayoApplication
import com.abiem.mayo.models.Task
import kotlinx.coroutines.runBlocking
import java.util.*
import java.util.concurrent.TimeUnit

private const val keyTaskId = "taskId"

interface ITaskExpiration {
    fun setupExpirationJob(expireTimeDelayMs: Long, taskID: String)
    fun clearExpirationJob()
}

class TaskExpirationWM : ITaskExpiration {
    private val wm get() = WorkManager.getInstance()
    private val tag = "TaskExpiration"

    override fun setupExpirationJob(expireTimeDelayMs: Long, taskID: String) {
        val inputData = Data.Builder()
                .putString(keyTaskId, taskID)
                .build()
        val request = OneTimeWorkRequestBuilder<TaskExpirationWorker>()
                .setConstraints(Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
                .setInputData(inputData)
                .setInitialDelay(expireTimeDelayMs, TimeUnit.MILLISECONDS)
                .addTag(tag)
                .build()
        wm.enqueue(request)
    }

    override fun clearExpirationJob() {
        wm.cancelAllWorkByTag(tag)
    }

//    val isExpirationJobActive: Boolean
//        get() {
//            val allStatuses = wm.getStatusesByTag(tag).value ?: return false
//            val statuses = allStatuses.filter { it.state != State.CANCELLED }
//            return when {
//                statuses.isEmpty() -> false
//                else -> statuses[0].state == State.ENQUEUED || statuses[0].state == State.RUNNING
//            }
//        }
}

class TaskExpirationAlarm : ITaskExpiration {
    private val appContext get() = MayoApplication.instance.applicationContext
    private val am get() = appContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    override fun setupExpirationJob(expireTimeDelayMs: Long, taskID: String) {
        logd("setup Alarm $taskID ${expireTimeDelayMs / 1000}")
        val intent = Intent(appContext, TaskExpirationService::class.java).apply {
            putExtra(keyTaskId, taskID)
        }
        val operation = PendingIntent.getService(appContext, 42, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val time = Calendar.getInstance().timeInMillis + expireTimeDelayMs
        AlarmManagerCompat.setExactAndAllowWhileIdle(am, AlarmManager.RTC_WAKEUP, time, operation)
    }

    override fun clearExpirationJob() {
        logd("clear Alarm")
        val intent = Intent(appContext, TaskExpirationService::class.java)
        val operation = PendingIntent.getService(appContext, 42, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        am.cancel(operation)
    }
}

class TaskExpirationService : IntentService("TaskExpirationService") {
    override fun onHandleIntent(intent: Intent) {
        TaskExpirationWorker.doActualWork(intent.getStringExtra(keyTaskId), applicationContext)
    }
}

object TaskExpiration : ITaskExpiration by TaskExpirationAlarm()
//object TaskExpiration : ITaskExpiration by TaskExpirationWM()

class TaskExpirationWorker(context: Context, params: WorkerParameters) : Worker(context, params) {
    companion object {
        fun doActualWork(taskId: String?, applicationContext: Context): Result {
            logd("Run work for $taskId")
            taskId ?: return Result.failure()
            return runBlocking {
                try {
                    val taskRef = FirebaseDatabase.getInstance().reference.child("tasks").child(taskId)
                    val task = taskRef.readValue<Task>()
                    if (!task.isCompleted) {
                        val timeUpdated = CommonUtility.convertStringToDateTime(task.timeUpdated)
                        val expireTime = Calendar.getInstance().apply {
                            time = timeUpdated
                            add(Calendar.MINUTE, Constants.expirationTimeInMinutes)
                        }
                        val timeNow = Calendar.getInstance()
                        if (expireTime.before(timeNow)) { // expired
                            logd("Task Expired")
                            task.isCompleted = true
                            task.timeUpdated = CommonUtility.getLocalTime()
                            task.completeType = applicationContext.getString(R.string.STATUS_FOR_TIME_EXPIRED)

                            taskRef.setValue(task)
                            CommonUtility.setTaskData(task, applicationContext)
                            CommonUtility.setTaskApplied(false, applicationContext)

                            // send notifications to users
                            val currentUserId = CommonUtility.getUserId(applicationContext)

                            logd("Get users")
                            val users = FirebaseDatabase.getInstance().reference
                                    .child("channels")
                                    .child(taskId)
                                    .child("users")
                                    .readValueGeneric(object : GenericTypeIndicator<Map<@JvmSuppressWildcards String, @JvmSuppressWildcards Long>>() {})
                            val userIds = users.keys.filter { it != currentUserId }
                            logd("users: $userIds")

                            val pushManager = PushNotificationManager()
                            userIds.forEach { userId ->
                                logd("reading user for $userId")
                                try {
                                    val token = FirebaseDatabase.getInstance().reference
                                            .child("users")
                                            .child(userId)
                                            .child("deviceToken")
                                            .readValue<String>()
                                    logd("Sending notification")
                                    pushManager.sendNotificationToDevice(token, task.taskDescription, task.taskID)
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                    logd("Can't get user's token")
                                }
                            }

                            logd("Showing local notification")
                            showLocalNotification(
                                    applicationContext.getString(R.string.notification_expired_message),
                                    applicationContext.getString(R.string.notification_help_message),
                                    applicationContext)

                            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(Intent(MapActivity.ACTION_TASK_EXPIRED))
                        } else {
                            var diff = expireTime.timeInMillis - timeNow.timeInMillis
                            diff = Math.min(diff, Constants.sTaskExpiryTime.toLong())
                            TaskExpiration.clearExpirationJob()
                            TaskExpiration.setupExpirationJob(diff, taskId)
                            logd("Task Rescheduled")
                        }
                    }

                    Result.success()
                } catch (e: Exception) {
                    e.printStackTrace()
                    logd("Task Exception")
                    showLocalNotification("Exception on worker. Retry", "Body: " + e.message, applicationContext)
                    Result.retry()
                }
            }
        }

        private fun showLocalNotification(pTitle: String, pBody: String, applicationContext: Context) {
            val builder = CommonUtility.notificationBuilder(applicationContext, pTitle, pBody)
            val contentIntent = PendingIntent.getActivity(
                    applicationContext,
                    1,
                    MayoApplication.instance.notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            builder.setContentIntent(contentIntent)

            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(0, builder.build())
        }
    }

    override fun doWork() = doActualWork(inputData.getString(keyTaskId), applicationContext)
}