package com.abiem.mayo.components

import com.google.firebase.database.*
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

private suspend fun <T : Any> awaitQuerySingleValue(query: Query, type: Class<T>): T =
        suspendCancellableCoroutine { continuation ->
            val listener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) = try {
                    continuation.resume(snapshot.getValue(type)!!)
                } catch (exception: Exception) {
                    continuation.resumeWithException(exception)
                }

                override fun onCancelled(error: DatabaseError) =
                        continuation.resumeWithException(error.toException())
            }

            query.addListenerForSingleValueEvent(listener)
            continuation.invokeOnCancellation { query.removeEventListener(listener) }
        }

suspend fun <T : Any> Query.readValue(type: Class<T>): T = awaitQuerySingleValue(this, type)

suspend inline fun <reified T : Any> Query.readValue(): T = readValue(T::class.java)


private suspend fun <T : Any> awaitQuerySingleValueGeneric(query: Query, type: GenericTypeIndicator<T>): T =
        suspendCancellableCoroutine { continuation ->
            val listener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) = try {
                    continuation.resume(snapshot.getValue(type)!!)
                } catch (exception: Exception) {
                    continuation.resumeWithException(exception)
                }

                override fun onCancelled(error: DatabaseError) =
                        continuation.resumeWithException(error.toException())
            }

            query.addListenerForSingleValueEvent(listener)
            continuation.invokeOnCancellation { query.removeEventListener(listener) }
        }

suspend fun <T : Any> Query.readValueGeneric(type: GenericTypeIndicator<T>): T = awaitQuerySingleValueGeneric(this, type)
