@file:JvmName("NotificationUtils")

package com.abiem.mayo.Utility

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build

const val CHANNEL_MAIN = "Main"

private val Context.notificationManager: NotificationManager
    get() = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

fun Context.buildNotificationChannels() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val chan = NotificationChannel(CHANNEL_MAIN, CHANNEL_MAIN, NotificationManager.IMPORTANCE_DEFAULT)
        chan.description = "Main channel"
        notificationManager.createNotificationChannel(chan)
    }
}