@file:JvmName("Utils")

package com.abiem.mayo.Utility

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.abiem.mayo.firebase.database.ValueEventListenerAdapter
import com.abiem.mayo.models.Message

private const val tag = "MAYO_LOGGER"
fun logd(msg: String) = Log.d(tag, msg)

fun mapMessageListToUserIds(mhelpMessageList: MutableList<Message>): List<String> {
    return mhelpMessageList.map { it.senderId }
}

fun loadMessagesForTask(taskId: String, userId: String, callback: (List<@JvmSuppressWildcards Message>) -> Unit) {
    FirebaseDatabase.getInstance().reference
            .child("channels")
            .child(taskId)
            .child("messages")
            .addListenerForSingleValueEvent(object : ValueEventListenerAdapter() {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val messages = dataSnapshot.children
                            .map { messageSnapshot ->
                                val messageHashList = messageSnapshot.value as? Map<*, *> ?: return
                                Message().apply {
                                    colorIndex = messageHashList["colorIndex"].toString()
                                    dateCreated = messageHashList["dateCreated"].toString()
                                    senderId = messageHashList["senderId"].toString()
                                    senderName = messageHashList["senderName"].toString()
                                    text = messageHashList["text"].toString()
                                    messageFromLocalDevice = Constants.MessageFromLocalDevice.no
                                    userType = Constants.UserType.OTHER
                                }
                            }
                            .filter { it.senderId != userId }
                    callback(messages)
                }
            })
}