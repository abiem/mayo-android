package com.abiem.mayo.backgroundservice;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.abiem.mayo.interfaces.LocationUpdationInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;


/**
 * Created by Lakshmikodali on 03/01/18.
 */

public class BackgroundLocationService extends Service implements
        LocationListener {

    private LocationRequest mLocationRequest;
    private LocationUpdationInterface mLocationUpdationInterface;
    private final IBinder mBinder = new LocalBinder();
    private PowerManager.WakeLock mWakeLock;
    private boolean servicesAvailable = false;

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(Constants.sUPDATE_INTERVAL);
        mLocationRequest.setSmallestDisplacement(Constants.sDisplacement);
        mLocationRequest.setFastestInterval(Constants.sFASTEST_INTERVAL);
        servicesAvailable = servicesConnected();

        requestLocationUpdates();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        start();

        if (!servicesAvailable)
            return START_STICKY;

        return START_STICKY;
    }


    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        return ConnectionResult.SUCCESS == resultCode;
    }


    public void start() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm != null) {
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "wakelock");
            mWakeLock.acquire(10 * 60 * 1000L /*10 seconds*/);
        }
    }


    public class LocalBinder extends Binder {
        public BackgroundLocationService getServerInstance() {
            return BackgroundLocationService.this;
        }
    }

    public void setCallbacks(LocationUpdationInterface callbacks) {
        mLocationUpdationInterface = callbacks;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.getFusedLocationProviderClient(this)
                .requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult location) {
                        updateLocation(location.getLastLocation());
                    }
                }, null);
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    private void updateLocation(Location location) {
        if (mLocationUpdationInterface != null) {
            mLocationUpdationInterface.updateLocation(location);
        }
    }

    @Override
    public void onDestroy() {
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
            this.mWakeLock = null;
        }
        super.onDestroy();
    }
}
