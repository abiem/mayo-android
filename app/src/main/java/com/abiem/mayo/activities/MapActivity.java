package com.abiem.mayo.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.github.sahasbhop.apngview.ApngDrawable;
import com.github.sahasbhop.apngview.ApngImageLoader;
import com.github.sahasbhop.apngview.assist.ApngImageLoaderCallback;
import com.github.sahasbhop.apngview.assist.ApngImageLoadingListener;
import com.github.sahasbhop.apngview.assist.ApngListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.abiem.mayo.R;
import com.abiem.mayo.Utility.CommonUtility;
import com.abiem.mayo.Utility.Constants;
import com.abiem.mayo.Utility.Utils;
import com.abiem.mayo.adapters.MapViewPagerAdapter;
import com.abiem.mayo.adapters.ThanksChatAdapter;
import com.abiem.mayo.application.MayoApplication;
import com.abiem.mayo.backgroundservice.BackgroundLocationService;
import com.abiem.mayo.classes.AnimateCard;
import com.abiem.mayo.classes.CardsDataModel;
import com.abiem.mayo.classes.CustomViewPager;
import com.abiem.mayo.classes.FakeMarker;
import com.abiem.mayo.classes.GeoFencing;
import com.abiem.mayo.classes.GeoFireClass;
import com.abiem.mayo.classes.MarkerClick;
import com.abiem.mayo.classes.ShownCardMarker;
import com.abiem.mayo.classes.UserLiveMarker;
import com.abiem.mayo.classes.ViewPagerScroller;
import com.abiem.mayo.components.TaskExpiration;
import com.abiem.mayo.firebase.database.FirebaseDatabase;
import com.abiem.mayo.firebase.database.TaskCallback;
import com.abiem.mayo.firebase.database.TaskExpireHandler;
import com.abiem.mayo.interfaces.LocationUpdationInterface;
import com.abiem.mayo.interfaces.OnItemClickListener;
import com.abiem.mayo.interfaces.ViewClickListener;
import com.abiem.mayo.models.CardLatlng;
import com.abiem.mayo.models.MapDataModel;
import com.abiem.mayo.models.MarkerTag;
import com.abiem.mayo.models.Message;
import com.abiem.mayo.models.Task;
import com.abiem.mayo.models.TaskLatLng;
import com.abiem.mayo.models.TaskLocations;
import com.abiem.mayo.models.UserMarker;
import com.abiem.mayo.models.UsersLocations;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

import static com.abiem.mayo.Utility.CommonUtility.isLocationEnabled;


import static android.content.ContentValues.TAG;
import android.provider.Settings.Secure;

@SuppressLint("Registered")
@EActivity(R.layout.activity_map)
public class MapActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        LocationUpdationInterface,
        ViewClickListener,
        GoogleMap.OnMapClickListener,
        TaskExpireHandler,
        ResultCallback<Status> {

    @App
    MayoApplication mMayoApplication;

    @ViewById(R.id.mapView)
    MapView mMapView;

    @ViewById(R.id.countbutton)
    Button mCountButton;

    @ViewById(R.id.viewPagerMapView)
    CustomViewPager mViewPagerMap;

    @ViewById(R.id.relativeMapLayout)
    RelativeLayout mRelativeMapLayout;

    @ViewById(R.id.imageHandsViewOnMapScreen)
    ImageView mImageHandsViewOnMap;

    @ViewById(R.id.rotateImageOnMapView)
    ImageView mRotateImageOnMapView;

    @ViewById(R.id.parentQuestLayout)
    LinearLayout mParentQuestLayout;

    @ViewById(R.id.questButton)
    Button questButton;

    @ViewById(R.id.cancelButton)
    Button cancelButton;

    @ViewById(R.id.loaderCard)
    CardView loaderCardView;

    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Double mCurrentLat, mCurrentLng;
    private GroundOverlay mCurrentLocationGroundOverlay;
    private Marker mCurrentLocationMarker;
    private ArrayList<UserMarker> mFakeUserMarker;
    private boolean isMarkerClick = false, isScrollingRight = false, isNewTaskEnter = false;
    private BackgroundLocationService mBackgroundLocationService;
    ArrayList<MapDataModel> mMapDataModels;
    Dialog mDialog;
    int positionNew;
    private MapViewPagerAdapter mMapViewPagerAdapter;
    private GeoFireClass mGeoFireClass;
    private GeoQuery mGeoQuery;
    private Location mCurrentLocation, mTaskLocation, mCurrentLocationForCardMarker;
    private FakeMarker mFakeMarker;
    private UserLiveMarker mUserLiveMarker;
    private HashSet<UserMarker> mNearByUsers, mAllNearByUsers;
    private ArrayList<TaskLocations> mTaskLocationsArray;
    private ArrayList<TaskLatLng> mTasksArray;
    private ShownCardMarker mShownCardMarker;
    private ViewPagerScroller mViewPagerScroller;
    private MarkerClick mMarkerClick;
    private float sumPositionAndPositionOffset = 0;
    private CardsDataModel mCardsDataModel;
    private FirebaseDatabase mFirebaseDatabase;
    private Date lastUpdateTime;
    private GeoFencing mGeoFencing;
    private GoogleReceiver mReceiver;
    private boolean isThanksDialogOpen = false;
    private BroadcastReceiver mBroadCastReceiver;
    public static boolean isFakeCardCompleted = false;
    private boolean cardsLoaded = false;
    private AnimateCard animateCard;
    private boolean flg = false;


//    String androidId = Settings.Secure.getString(getContentResolver(),
//            Settings.Secure.ANDROID_ID);
//    //[firebase auth]
//    private FirebaseAuth mAuth;
    ActionCodeSettings actionCodeSettings;
    String overallEmail = "epd80687@eveav.com";
    FirebaseAuth mYauth = FirebaseAuth.getInstance();

    @AfterViews
    protected void init() {
        getFirebaseInstance();
        initThanksAnimation();

        mMayoApplication.setActivity(this);
        mMapView.onCreate(null);
        mNearByUsers = new HashSet<>();
        mAllNearByUsers = new HashSet<>();
        mTaskLocationsArray = new ArrayList<>();
        mTasksArray = new ArrayList<>();
        registerReceiver();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mFakeUserMarker = new ArrayList<>();
        mRelativeMapLayout.setFitsSystemWindows(true);
        setDataModel();
        setTouchListenerOfViewPager();
        checkGoogleServiceAvailable();
        setGeoFire();
        mCountButton.setText(String.valueOf(CommonUtility.getPoints(this)));
        mCountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPointView();
            }
        });
        if (CommonUtility.getFakeCardOne(this) && CommonUtility.getFakeCardTwo(this) && CommonUtility.getFakeCardThree(this)) {
            CommonUtility.setFakeCardShownOrNot(false, this);
            if (!cardsLoaded) {
                animateCard = new AnimateCard(loaderCardView);
                animateCard.playFadeInOutAnimation();
            }
        }
        if (mGeoFencing == null) {
            mGeoFencing = new GeoFencing(this);
        }

        // TODO: 01/08/2018 fetch active tasks from the UserData object and add them to adapter #LocationUpdate
        // todo loaded data is not used? #LocationUpdate
        mFirebaseDatabase.getTaskParticipatedByUsers(CommonUtility.getUserId(this));
        // todo why do we loading all users?
        mFirebaseDatabase.getUserIdsFromFirebase();
        mFirebaseDatabase.removePointListener();
        mFirebaseDatabase.getPointsFromFirebase(CommonUtility.getUserId(this));
        if (CommonUtility.getTaskApplied(this)) {
            Task task = CommonUtility.getTaskData(this);
            mFirebaseDatabase.getAllMessagesOfTaskEnteredByUser(task.getTaskID());
        }
        setNotificationReceiverBroadcast();
        registerExpirationEventReceiver();
        checkIfTheresExpiredTask();
        //[firebase auth]
        // Initialize Firebase Auth
//        mAuth = FirebaseAuth.getInstance();

    }

    private void checkTaskExpired(Intent intent) {
        if (intent.hasExtra(Constants.Notifications.sNotification_Type)) {
            String typeStr = intent.getStringExtra(Constants.Notifications.sNotification_Type);
            int type = Integer.parseInt(typeStr);

            if (type == Constants.Notifications.sNOTIFICATION_NEARBY_TASK) {
                String id = intent.getStringExtra(Constants.Notifications.sTaskId);
                mFirebaseDatabase.loadTask(id, new TaskCallback() {
                    @Override
                    public void onTaskLoaded(Task task) {
                        if (task.isCompleted()) {
                            getIntent().removeExtra(Constants.Notifications.sNotification_Type);
                            taskExpireAlert();
                        }
                    }
                });
            }
        }
    }

    private void initThanksAnimation() {
        String uri = "assets://apng/fist_bump_720p.png";
        ApngImageLoader.getInstance().displayImage(uri, mImageHandsViewOnMap,
                new ApngImageLoadingListener(this, Uri.parse(uri), new ApngImageLoaderCallback() {
                    @Override
                    public void onLoadFinish(boolean success, String imageUri, View view) {
                        if (getIntent().hasExtra(Constants.Notifications.sNotification_Type)) {
                            String typeStr = getIntent().getStringExtra(Constants.Notifications.sNotification_Type);
                            int type = Integer.parseInt(typeStr);
                            if (type == Constants.Notifications.sNOTIFICATION_WERE_THANKS) {
                                getIntent().removeExtra(Constants.Notifications.sNotification_Type);
                                animateHands();
                            }
                        }
                    }
                }));
    }

    private void finishAnimatingLoader() {
        if (animateCard != null) {
            animateCard.finishAnimation();
            animateCard = null;
        }
    }

    public void setViewPagerData() {
        setViewPager();
        if (mViewPagerScroller == null) {
            scrollViewPager();
        }
        if (CommonUtility.getTaskApplied(this) && !CommonUtility.getTaskData(this).isCompleted()) {
            getFirebaseInstance();
            mFirebaseDatabase.setUpdateTimeOfCurrentTask(CommonUtility.getTaskData(this).getTaskID());
            mFirebaseDatabase.setListenerForEnteringTask(CommonUtility.getTaskData(this).getTaskID());
        }
        cardsLoaded = true;
        finishAnimatingLoader();
    }

    private void setGeoFire() {
        mGeoFireClass = new GeoFireClass(this);
        mGeoFireClass.setGeoFire();
    }

    private void checkGoogleServiceAvailable() {
        if (CommonUtility.googleServicesAvailable(this)) {
            mMapView.getMapAsync(MapActivity.this);
        } else {
            mMayoApplication.showToast(this, getResources().getString(R.string.notsupported));
        }
    }

    private boolean checkPermissions() {
        boolean isPermission = true;
        String permission[] = CommonUtility.checkPermissions();
        for (String aPermission : permission) {
            if (ContextCompat.checkSelfPermission(this, aPermission) != PackageManager.PERMISSION_GRANTED) {
                locationNotEnabled();
                disableLocationDialogView(Constants.PermissionDialog.AppPermissionDialog.ordinal());
                isPermission = false;
                break;
            }
        }
        return isPermission;
    }


    private void showPointView() {
        if (!isFinishing()) {
            final Dialog dialog = CommonUtility.showCustomDialog(this, R.layout.score_screen);
            if (dialog != null) {
                ImageButton deleteView = dialog.findViewById(R.id.delete_view);
                TextView score = dialog.findViewById(R.id.points);
                TextView feedback = dialog.findViewById(R.id.feedback_link);
                deleteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                score.setText(String.valueOf(CommonUtility.getPoints(MapActivity.this)));
                feedback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(MapActivity.this, FeedbackActivity.class);
                        startActivity(intent);
                    }
                });
            }

            getCurrentLocation();

            // try send email verification to my email


            if (!flg) {
                buildActionCodeSettings();
                sendSignInLink(overallEmail, actionCodeSettings);
            }
//            else{
//                getDynamic();
////                verifySignInLink();
//            }




//            if (!verifySignInLink()) {
//                buildActionCodeSettings();
//                sendEmail();
//            } else {
//                Toast.makeText(MapActivity.this, "email verified",Toast.LENGTH_LONG).show();
//            }
        }
    }

    //------------------
//    @Override
//    public void onStart() {
//        super.onStart();
//        // Check if user is signed in (non-null) and update UI accordingly.
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
//    }
//
//    private void updateUI(FirebaseUser curUser) {
//
//    }
    public void buildActionCodeSettings() {
        // [START auth_build_action_code_settings]
        actionCodeSettings =
                ActionCodeSettings.newBuilder()
                        // URL you want to redirect back to. The domain (www.example.com) for this
                        // URL must be whitelisted in the Firebase Console.
//                        .setUrl("https://heymayo.page.link/Eit5")
//                        .setUrl("https://www.google.com/")
                        .setUrl("https://www.heymayo.com/")
                        // This must be true
                        .setHandleCodeInApp(true)
                        .setAndroidPackageName(
                                "com.abiem.mayo",
                                true, /* installIfNotAvailable */
                                "12"    /* minimumVersion */)
                        .build();
        // [END auth_build_action_code_settings]
    }
    // sent authentication link

    public void sendEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Log.d(TAG, "users email: " + user.getEmail());



        user.updateEmail("d748086@urhen.com")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User email address updated.");
                        } else {
                            Log.d(TAG, "not updated");
                        }
                    }
                });
        user.sendEmailVerification(actionCodeSettings)
                .addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task task) {
                        // Re-enable button
                        if (task.isSuccessful()) {
                            Toast.makeText(MapActivity.this,
                                    "Verification email sent to " + "user",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(MapActivity.this,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    public void sendSignInLink(String email, ActionCodeSettings actionCodeSettings) {
        // [START auth_send_sign_in_link]
        try{


            //---------
            FirebaseAuth auth = FirebaseAuth.getInstance();
            FirebaseUser user = auth.getCurrentUser();
            if (auth == null) System.out.println("auth is null is true");
            if (actionCodeSettings == null) System.out.println("action is null");
            if (email == null) System.out.println("email is null");
//            auth.sendSignInLinkToEmail(email, actionCodeSettings);

            auth.sendSignInLinkToEmail(email, actionCodeSettings)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Email sent.");
                                System.out.println("emial sent");
                                flg = true;
                            }else {
                                Toast.makeText(MapActivity.this, task.getException().getMessage(),Toast.LENGTH_LONG).show();
                                System.out.println("email not sent");
                            }
                        }
                    });
        } catch (Exception e) {
            e.getStackTrace()[0].getLineNumber();
            System.out.println(e);
        }

        // [END auth_send_sign_in_link]
    }

    public void getDynamic() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }
                        if (deepLink != null) {
                            Log.d(TAG, deepLink + "");
                            Log.d(TAG, deepLink.getHost() + "\t host");
                            Log.d(TAG, deepLink.getPath() + "\t path");
//                            verifySignInLink();
                            String emailLink = deepLink.toString();
                            FirebaseAuth auth = FirebaseAuth.getInstance();
                            if (auth.isSignInWithEmailLink(emailLink)) {
                                // Retrieve this from wherever you stored it
                                String email = overallEmail;

                                // The client SDK will parse the code from the link for you.
                                auth.signInWithEmailLink(email, emailLink)
                                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull com.google.android.gms.tasks.Task<AuthResult> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d("MapActivity", "Successfully signed in with email link!");
                                                    AuthResult result = task.getResult();
                                                    Toast.makeText(MapActivity.this, "email verified",Toast.LENGTH_LONG).show();

                                                    // You can access the new user via result.getUser()
                                                    // Additional user info profile *not* available via:
                                                    // result.getAdditionalUserInfo().getProfile() == null
                                                    // You can check if the user is new or existing:
                                                    // result.getAdditionalUserInfo().isNewUser()
                                                } else {
                                                    Log.e("MapActivity", "Error signing in with email link", task.getException());
                                                    Toast.makeText(MapActivity.this, "email not verified",Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                            }
                        } else Log.d(TAG, "deep link is null");

                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });
        return;
    }
    public void verifySignInLink() {
        // [START auth_verify_sign_in_link]
        Log.d(TAG, "in verify sing in link");

        try{
//            Toast.makeText(MapActivity.this, "verifying email",Toast.LENGTH_LONG).show();

            FirebaseAuth auth = FirebaseAuth.getInstance();
            Intent intent = getIntent();
            if (intent.getData() == null) {
                Log.d(TAG, "intent data is null");
                return;
            }
            String emailLink = intent.getData().toString();
            Log.d(TAG, "emailLink: " + emailLink);
            if (intent == null) Log.d(TAG, "intent is null");
            if (intent.getData() == null) Log.d(TAG, "data is null");
            if (auth == null) Log.d(TAG, "auth is null");

            // Confirm the link is a sign-in with email link.
            if (auth.isSignInWithEmailLink(emailLink)) {
                // Retrieve this from wherever you stored it
                String email = overallEmail;

                // The client SDK will parse the code from the link for you.
                auth.signInWithEmailLink(email, emailLink)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull com.google.android.gms.tasks.Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Log.d("MapActivity", "Successfully signed in with email link!");
                                    AuthResult result = task.getResult();
                                    Toast.makeText(MapActivity.this, "email verified",Toast.LENGTH_LONG).show();

                                    // You can access the new user via result.getUser()
                                    // Additional user info profile *not* available via:
                                    // result.getAdditionalUserInfo().getProfile() == null
                                    // You can check if the user is new or existing:
                                    // result.getAdditionalUserInfo().isNewUser()
                                } else {
                                    Log.e("MapActivity", "Error signing in with email link", task.getException());
                                    Toast.makeText(MapActivity.this, "email not verified",Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        } catch (Exception e) {
            System.out.println("error at line:" + e.getStackTrace()[0].getLineNumber());
            System.out.println("error: " + e);
        }
        return;
        // [END auth_verify_sign_in_link]
    }

    //------------------

    public void getCurrentLocation() {
        getCurrentLocationInternal(mGoogleMap.getCameraPosition().zoom);
    }

    private void getCurrentLocationInternal(float zoom) {
        if (mGoogleMap != null && mCurrentLocation != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()))
                    .zoom(zoom).build();
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setTouchListenerOfViewPager() {
        mViewPagerMap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isMarkerClick = false;
                return false;
            }
        });
    }

    private void scrollViewPager() {
        mViewPagerScroller = new ViewPagerScroller(this, mViewPagerMap, mMapDataModels,
                mMapViewPagerAdapter, mShownCardMarker, mCurrentLocationForCardMarker, mGoogleMap);

        mViewPagerMap.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                isScrollingRight = !(position + positionOffset > sumPositionAndPositionOffset);
                sumPositionAndPositionOffset = position + positionOffset;
            }

            @Override
            public void onPageSelected(int position) {
                int Type = mMapDataModels.get(position).getFakeCardPosition();
                int value;
                Constants.CardType cardType = Constants.CardType.values()[Type];
                if (position != Constants.CardType.POST.getValue()) {
                    if (CommonUtility.getSoftKeyBoardState(MapActivity.this)) {
                        mMayoApplication.hideKeyboard(getCurrentFocus());
                        CommonUtility.setSoftKeyBoardState(false, MapActivity.this);
                    }
                }
                if (!isMarkerClick) {
                    switch (cardType) {
                        case POST:
                            value = mViewPagerScroller.getPostCard();
                            if (value != -1) {
                                mCountButton.setText(String.valueOf(value));
                                getFirebaseInstance();
                                mFirebaseDatabase.updatePointsAtFirebaseServer(CommonUtility.getUserId(MapActivity.this), true);
                            }
                            mMapViewPagerAdapter.setTaskCardViewVisible();
                            break;
                        case FAKECARDONE:
                            isFakeCardCompleted = false;
                            mViewPagerScroller.getFakeCardOne();
                            break;
                        case FAKECARDTWO:
                            isFakeCardCompleted = false;
                            try {
                                value = mViewPagerScroller.getFakeCardTwo(isScrollingRight);
                                if (value != -1) {
                                    mCountButton.setText(String.valueOf(value));
                                    getFirebaseInstance();
                                    mFirebaseDatabase.updatePointsAtFirebaseServer(CommonUtility.getUserId(MapActivity.this), true);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                mMayoApplication.showToast(MapActivity.this, "Exception occur on scroll fake card one");
                            }
                            break;
                        case FAKECARDTHREE:
                            isFakeCardCompleted = false;
                            value = mViewPagerScroller.getFakeCardThree(isScrollingRight);
                            if (value != -1) {
                                mCountButton.setText(String.valueOf(value));
                                getFirebaseInstance();
                                mFirebaseDatabase.updatePointsAtFirebaseServer(CommonUtility.getUserId(MapActivity.this), true);
                            }
                            break;
                        case DEFAULT:
                            isFakeCardCompleted = true;
                            mViewPagerScroller.setLiveCardViewMarker(mViewPagerMap.getCurrentItem());
                            getFirebaseInstance();
                            if (mMapDataModels.get(mViewPagerMap.getCurrentItem()).getTaskLatLng() != null) {
                                String timeStamp = mMapDataModels.get(mViewPagerMap.getCurrentItem()).getTaskLatLng().getTask().getTaskID();
                                mFirebaseDatabase.setTaskViewsByUsers(timeStamp);
                            }
                            break;
                    }
                    positionNew = position;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setDataModel() {
        if (mCardsDataModel == null) {
            mCardsDataModel = new CardsDataModel(this, mTasksArray);
        }
        if (mMapDataModels == null) {
            mMapDataModels = mCardsDataModel.getDataModel();
        }
    }

    private void setViewPager() {
        mViewPagerMap.setPagingEnabled(true);
        mViewPagerMap.setPageMargin(Constants.CardMarginSetValues.sMarginValue);
        mMapViewPagerAdapter = new MapViewPagerAdapter(this, mMapDataModels, this, mViewPagerMap, this, mMayoApplication);
        if (mCardsDataModel != null) {
            mCardsDataModel.setViewPagerAdapter(mMapViewPagerAdapter);
        }
        if (mShownCardMarker != null) {
            mShownCardMarker.getAnotherUsersLiveMarker();
        }
        mViewPagerMap.setAdapter(mMapViewPagerAdapter);
        if (!CommonUtility.getFakeCardShownOrNot(this)) {
            mViewPagerMap.setCurrentItem(0);
            new com.abiem.mayo.Utility.CountDown(this, 500, 100, mViewPagerMap, mMapDataModels);
        } else if (mMapDataModels.size() > 1) {
            mViewPagerMap.setCurrentItem(1);
        }
        positionNew = mViewPagerMap.getCurrentItem();
    }

    private void setCardMarkers(Location pLocation) {
        mShownCardMarker = new ShownCardMarker(this, mMapDataModels, pLocation, mGoogleMap);
        mShownCardMarker.getCardMarkers();
        setOnMarkerClickListener();
        setTaskMarker();
    }

    private void setTaskMarker() {
        if (CommonUtility.getTaskApplied(this)) {
            mShownCardMarker.setTaskMarker(CommonUtility.getTaskLocation(this));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkTaskExpired(intent);
        checkExpiredNotificationReceived(intent);
    }

    private void checkExpiredNotificationReceived(Intent intent) {
        if (intent.hasExtra(Constants.Notifications.sNotification_Type)) {
            String typeStr = intent.getStringExtra(Constants.Notifications.sNotification_Type);
            int type = Integer.parseInt(typeStr);

            if (type == Constants.Notifications.sNOTIFICATION_TOPIC_COMPLETED) {
                getIntent().removeExtra(Constants.Notifications.sNotification_Type);

                String id = intent.getStringExtra(Constants.Notifications.sChannelId);
                Intent i = new Intent(Intent.ACTION_MAIN);
                i.putExtra(Constants.Notifications.sChannelId, id);
                sendBroadcast(i);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (flg) {
            getDynamic();
//            verifySignInLink();
//        }


        checkTaskExpired(getIntent());
        checkExpiredNotificationReceived(getIntent());
        mMapView.onResume();
        /*
         * Reload points from Shared Preferences-
         * */
        if (CommonUtility.getHandsAnimationShownOnMap(MapActivity.this)) {
            final ApngDrawable mApngDrawable = ApngDrawable.getFromView(mImageHandsViewOnMap);
            if (mApngDrawable != null) {
                mImageHandsViewOnMap.setVisibility(View.VISIBLE);
                mRotateImageOnMapView.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(MapActivity.this, R.anim.rotate);
                mRotateImageOnMapView.startAnimation(animation);
                CommonUtility.setHandsAnimationShownOnMap(false, MapActivity.this);
                mRelativeMapLayout.setFitsSystemWindows(false);
                mApngDrawable.setNumPlays(1);
                mApngDrawable.setShowLastFrameOnStop(true);
                mApngDrawable.setApngListener(new ApngListener() {
                    @Override
                    public void onAnimationEnd(ApngDrawable apngDrawable) {
                        finishAnimating(mApngDrawable);
                    }
                });
                mApngDrawable.start();
            }
        }
        if (mGoogleMap != null) {
            if (mDialog != null && checkPermissions()) {
                mDialog.dismiss();
                mDialog = null;
            }
            checkLocationEnabledOrNot();
        }
    }

    public static String ACTION_TASK_EXPIRED = "com.abiem.mayo.action.TASK_EXPIRED";

    BroadcastReceiver taskExpiredReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Utils.logd("Broadcast: " + ACTION_TASK_EXPIRED);
            onReceiveTaskExpiredEvent();
        }
    };

    private void onReceiveTaskExpiredEvent() {
        if(mMapViewPagerAdapter == null) { // activity recreated
            return;
        }
        mMapViewPagerAdapter.setPostMessageView();
        updateTaskDataDueToTaskExpired(CommonUtility.getTaskData(this), mFirebaseDatabase.getLastFiveMessagesFromMessagesList());
    }

    private void registerExpirationEventReceiver() {
        IntentFilter intentFilter = new IntentFilter(ACTION_TASK_EXPIRED);
        LocalBroadcastManager.getInstance(this).registerReceiver(taskExpiredReceiver, intentFilter);
    }

    private void unregisterExpirationEventReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(taskExpiredReceiver);
    }

    private void checkIfTheresExpiredTask() {
        try {
            final Task task = CommonUtility.getTaskData(this);
            if(task.isCompleted() && task.getCompleteType().equals(getString(R.string.STATUS_FOR_TIME_EXPIRED))) {
                Utils.loadMessagesForTask(task.getTaskID(), CommonUtility.getUserId(this), new Function1<List<Message>, Unit>() {
                    @Override
                    public Unit invoke(List<Message> messages) {
                        Utils.logd("Loaded messages: " + messages);
                        updateTaskDataDueToTaskExpired(task, messages);
                        return null;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.logd("No task saved");
        }
    }

    private void checkLocationEnabledOrNot() {
        if (!isLocationEnabled(this)) {
            locationNotEnabled();
            disableLocationDialogView(Constants.PermissionDialog.LocationDialog.ordinal());
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMayoApplication.hideKeyboard(getCurrentFocus());
        CommonUtility.setSoftKeyBoardState(false, MapActivity.this);
        if (mMapViewPagerAdapter != null)
            mMapViewPagerAdapter.setPostCardText();
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (!status.isSuccess()) {
            String errorMessage = mGeoFencing.getErrorString(this, status.getStatusCode());
            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    private void finishAnimating(Animatable mApngDrawable) {
        mRotateImageOnMapView.setVisibility(View.GONE);
        mRotateImageOnMapView.clearAnimation();
        getFirebaseInstance();
        if (mMapViewPagerAdapter != null) {
            for (int i = 0; i < mMapDataModels.size(); i++) {
                if (mMapDataModels.get(i).getFakeCardPosition() == Constants.CardType.FAKECARDTWO.getValue()) {
                    mMapViewPagerAdapter.deleteItemFromArrayList(i);
                    mMapDataModels.get(i).getCardLatlng().getMarker().remove();
                    int value = CommonUtility.getPoints(MapActivity.this) + 1;
                    CommonUtility.setPoints(value, MapActivity.this);
                    mCountButton.setText(String.valueOf(value));
                    mFirebaseDatabase.updatePointsAtFirebaseServer(CommonUtility.getUserId(MapActivity.this), true);
                    CommonUtility.setFakeCardTwo(true, MapActivity.this);
                }
                if (mMapDataModels.get(i).getFakeCardPosition() == Constants.CardType.FAKECARDTHREE.getValue()) {
                    CardLatlng cardLatlng = mMapDataModels.get(i).getCardLatlng();
                    cardLatlng.getMarker().setIcon(mShownCardMarker.getIconLarge());
                    cardLatlng.getMarker().setZIndex(Constants.sMarkerZIndexMaximum);
                    mShownCardMarker.setGoogleMapPosition(mCurrentLocationForCardMarker, cardLatlng);
                    break;
                }
            }
        }
        mRelativeMapLayout.setFitsSystemWindows(true);
        if (mApngDrawable == null) {
            mImageHandsViewOnMap.setVisibility(View.GONE);
            return;
        }
        if (mApngDrawable.isRunning()) {
            mApngDrawable.stop(); // Stop animation
        }
        mImageHandsViewOnMap.setVisibility(View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        MapsInitializer.initialize(this);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.setBuildingsEnabled(true);
        if (!isLocationEnabled(this)) {
            locationNotEnabled();
            disableLocationDialogView(Constants.PermissionDialog.LocationDialog.ordinal());
            return;
        }
        checkPermissions();
        mGoogleMap.setOnMapClickListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(MapActivity.this).
                addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        mLocationRequest = LocationRequest.create();
                        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        getLocation();
                        startService();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                }).enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                }).build();
        mGoogleApiClient.connect();
    }


    private void locationNotEnabled() {
        if (mGoogleMap != null) {
            LatLng latLng = CommonUtility.getUserLocation(this);
            if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, Constants.sKeyCameraZoom));
            }
        }
    }

    private void startService() {
        // Starting a service for update location in background
        Intent serviceIntent = new Intent(MapActivity.this, BackgroundLocationService.class);
        startService(serviceIntent);
        Intent intent = new Intent(this, BackgroundLocationService.class);
        getApplicationContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * Callbacks for service binding, passed to bindService()
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get MyService instance
            BackgroundLocationService.LocalBinder binder = (BackgroundLocationService.LocalBinder) service;
            mBackgroundLocationService = binder.getServerInstance();
            mBackgroundLocationService.setCallbacks(MapActivity.this); // register
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

        }
    };

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                if (location == null && getApplicationContext() != null) {
                    mMayoApplication.showToast(getApplicationContext(), getResources().getString(R.string.cantgetlocation));
                } else {
                    if (location != null) {
                        if (mCurrentLocationForCardMarker == null) {
                            mCurrentLocationForCardMarker = location;
                        }
                        setCurrentLocation(location);
                        sendDataToFirebaseOfUserLocation(location);
                        if (mShownCardMarker == null) {
                            setCardMarkers(location);
                        }
                        if (mGeoQuery == null) {
                            setGeoQuery();
                        }
                    }
                }
            }
        });
    }

    /**
     * set current location of user and add marker and circle on current location
     *
     * @param location current location of user
     */
    private void setCurrentLocation(Location location) {
        mCurrentLocation = location;
        setUpGeoFencingForTask(location.getLatitude(), location.getLongitude());
        CommonUtility.setUserLocation(mCurrentLocation, this);
        LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
        mCurrentLat = location.getLatitude();
        mCurrentLng = location.getLongitude();
        CameraPosition cameraPosition = new CameraPosition(ll, 45, 45, 45);
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mGoogleMap.animateCamera(update);
        if (mCurrentLocationGroundOverlay == null) {
            drawCircle(location);
        }
        if (mCurrentLocationMarker == null) {
            addCurrentLocationMarker(location);
        }
    }

    private void drawCircle(Location location) {
        mCurrentLocation = location;
        setGroundOverlayForShowingCircle(mCurrentLocation);
    }

    private void setOnMarkerClickListener() {
        mMarkerClick = new MarkerClick(this, mViewPagerMap, mMapDataModels);
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTag() != null && mViewPagerScroller != null) {
                    isMarkerClick = true;
                    if (marker.getTag().toString().equals(String.valueOf(Constants.CardType.FAKECARDTHREE.getValue()))
                            || marker.getTag().toString().equals(String.valueOf(Constants.CardType.FAKECARDONE.getValue()))
                            || marker.getTag().toString().equals(String.valueOf(Constants.CardType.FAKECARDTWO.getValue()))
                            || marker.getTag().toString().equals(String.valueOf(Constants.CardType.POST.getValue()))) {
                        int markerGet = Integer.parseInt(marker.getTag().toString());
                        Constants.CardType cardType = Constants.CardType.values()[markerGet];
                        switch (cardType) {
                            case POST:
                                mMarkerClick.getPostCardMarker();
                                mViewPagerScroller.setFakeCardLargeIcon(Constants.CardType.POST.getValue());
                                mViewPagerScroller.clearExpireCardMarker();
                                break;
                            case FAKECARDONE:
                                mMarkerClick.getFirstFakeCard();
                                mViewPagerScroller.setFakeCardLargeIcon(Constants.CardType.FAKECARDONE.getValue());
                                mViewPagerScroller.clearExpireCardMarker();
                                break;
                            case FAKECARDTWO:
                                mMarkerClick.getSecondFakeCard();
                                mViewPagerScroller.setFakeCardLargeIcon(Constants.CardType.FAKECARDTWO.getValue());
                                mViewPagerScroller.clearExpireCardMarker();
                                break;
                            case FAKECARDTHREE:
                                mMarkerClick.getThirdFakeCard();
                                mViewPagerScroller.setFakeCardLargeIcon(Constants.CardType.FAKECARDTHREE.getValue());
                                mViewPagerScroller.clearExpireCardMarker();
                                break;
                        }
                    } else {
                        if (marker.getTag() != null && marker.getTag() instanceof MarkerTag) {
                            int count = 0;
                            if (CommonUtility.getFakeCardOne(MapActivity.this)) {
                                count += 1;
                            }
                            if (CommonUtility.getFakeCardTwo(MapActivity.this)) {
                                count += 1;
                            }
                            if (CommonUtility.getFakeCardThree(MapActivity.this)) {
                                count += 1;
                            }
                            MarkerTag markerTag = ((MarkerTag) marker.getTag());
                            int position = Integer.parseInt(markerTag.getIdNew()) - count;
                            mMarkerClick.getCardMarker(position);
                            mViewPagerScroller.setLiveUserMarkerLarge(position);
                            mViewPagerScroller.clearExpireCardMarker();
                        }
                    }
                }
                return true;
            }
        });
    }

    private void setGroundOverlayForShowingCircle(Location location) {
        Bitmap bitmap = loadCircleBitmap(R.drawable.location_circle_radius);
        BitmapDescriptor currentLocationImage = BitmapDescriptorFactory.fromBitmap(bitmap);
        GroundOverlayOptions groundOverlayOptions = new GroundOverlayOptions();
        groundOverlayOptions.position(new LatLng(location.getLongitude(), location.getLatitude()), 200f * 2);
        groundOverlayOptions.image(currentLocationImage).transparency(0.2f);
        mCurrentLocationGroundOverlay = mGoogleMap.addGroundOverlay(groundOverlayOptions);
        mCurrentLocationGroundOverlay.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    private void addCurrentLocationMarker(Location location) {
        Bitmap bitmap = loadBitmap(R.drawable.location_circle);
        BitmapDescriptor currentLocationIcon = BitmapDescriptorFactory.fromBitmap(bitmap);
        MarkerOptions currentLocationMarker = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title(getResources().getString(R.string.my_location))
                .icon(currentLocationIcon);
        mCurrentLocationMarker = mGoogleMap.addMarker(currentLocationMarker);
    }

    private void setFakeMarker(Location location) {
        if (mFakeUserMarker != null && mFakeUserMarker.size() > 0) {
            //remove all previous fake markers
            try {
                for (UserMarker userMarker : mFakeUserMarker) {
                    if (userMarker.getMarker() != null) {
                        userMarker.getMarker().remove();
                    }
                }
                mFakeUserMarker.clear();
            } catch (Exception e) {
                e.printStackTrace();
                mMayoApplication.showToast(this, "remove marker exception");
            }
        }
        int numOfFakeUsers = FakeMarker.generateRandomMarkerOfFakeUsers();
        for (int i = 0; i < numOfFakeUsers; i++) {
            addFakeUserMaker(location);
        }
        if (mFakeUserMarker.size() > 0) {
            Log.d("fake_user_marker_add:", String.valueOf(mFakeUserMarker.size()));
            CommonUtility.setFakeMarkerShown(true, MapActivity.this);
            mFakeMarker = new FakeMarker(MapActivity.this, mFakeUserMarker);
            mFakeMarker.startTimer();
        }
    }

    private Bitmap loadBitmap(@DrawableRes int res) {
        Drawable drawable = ContextCompat.getDrawable(this, res);
        return CommonUtility.drawableToBitmap(drawable);
    }

    private Bitmap loadCircleBitmap(@DrawableRes int res) {
        Drawable drawable = ContextCompat.getDrawable(this, res);
        return CommonUtility.drawableToBitmapForCircle(drawable);
    }

    private void addFakeUserMaker(Location location) {
        Bitmap bitmap = loadBitmap(R.drawable.location_fake_users_circle);
        BitmapDescriptor fakeLocationIcon = BitmapDescriptorFactory.fromBitmap(bitmap);
        double lat, lng;
        int latlngContainNumber = FakeMarker.generateRandomLocationOfFakeUser();
        lat = FakeMarker.fakeUserChoices[latlngContainNumber][0];
        lng = FakeMarker.fakeUserChoices[latlngContainNumber][1];
        LatLng latLng = new LatLng(location.getLatitude() + lat, location.getLongitude() + lng);
        MarkerOptions fakeMarker = new MarkerOptions().position(latLng)
                .icon(fakeLocationIcon);
        setFakeUserModel(fakeMarker, latLng);
    }

    public void removeFakeMarkerAccodingToTime(int pIndex) {
        if (mFakeUserMarker.size() > 0) {
            mFakeUserMarker.get(pIndex).getMarker().remove();
            mFakeUserMarker.remove(pIndex);
            Log.d("fake_user_marker_timer:", String.valueOf(pIndex));
        }
        if (mFakeUserMarker.size() == 0) {
            CommonUtility.setFakeMarkerShown(true, MapActivity.this);
            if (mFakeMarker != null) {
                mFakeMarker.stoptimertask();
            }
        }
    }

    public void removeUserMarkerAccordingToTime(UserMarker pUserMarker) {
        if (mNearByUsers.size() > 0) {
            pUserMarker.getMarker().remove();
            mNearByUsers.remove(pUserMarker);
        }
        if (mNearByUsers.size() == 0) {
            if (mUserLiveMarker != null) {
                mUserLiveMarker.stoptimertask();
                mUserLiveMarker = null;
            }
        }
    }

    private void setFakeUserModel(MarkerOptions pFakeMarker, LatLng pLatlng) {
        UserMarker fakeUsersShown = new UserMarker();
        fakeUsersShown.setMarker(mGoogleMap.addMarker(pFakeMarker));
        fakeUsersShown.setLatLng(pLatlng);
        fakeUsersShown.setStartTime(CommonUtility.getCurrentTime());
        fakeUsersShown.setEndTime(CommonUtility.getEndTimeOfFakeUsers(FakeMarker.generateEndTimeOfMarkerShown()));
        mFakeUserMarker.add(fakeUsersShown);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        if (mFakeMarker != null) {
            mFakeMarker.stoptimertask();
            CommonUtility.setFakeMarkerShown(false, MapActivity.this);
        }
        if (mUserLiveMarker != null) {
            mUserLiveMarker.stoptimertask();
        }
        if (mReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        }
        if (this.mBroadCastReceiver != null) {
            unregisterReceiver(this.mBroadCastReceiver);
            this.mBroadCastReceiver = null;
        }
        unregisterExpirationEventReceiver();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void updateLocation(Location location) {
        if (mCurrentLocationMarker != null) {
            mCurrentLocationMarker.remove();
        }
        mCurrentLocation = location;
        CommonUtility.setUserLocation(mCurrentLocation, this);
        addCurrentLocationMarker(mCurrentLocation);
        sendDataToFirebaseOfUserLocation(location);
        Date currentTime = CommonUtility.getCurrentTime();
        getFirebaseInstance();
        if (lastUpdateTime == null) {
            lastUpdateTime = currentTime;
            sendUsersDataToFirebase();
        } else {
            lastUpdateTime = currentTime;
            mFirebaseDatabase.writeNewUserLocation(CommonUtility.getUserId(this), mCurrentLocation);
        }
    }

    private void sendUsersDataToFirebase() {
        boolean fakeCardShownOrNot = CommonUtility.getFakeCardShownOrNot(this);
        mFirebaseDatabase.writeNewUserData(
                CommonUtility.getUserId(this),
                CommonUtility.getLocalTime(),
                CommonUtility.getDeviceToken(this),
                !fakeCardShownOrNot,
                mCurrentLocation
        );
    }

    /**
     * send current location data to firebase
     *
     * @param location current location of user send to firebase using geoquery
     */

    private void sendDataToFirebaseOfUserLocation(Location location) {
        mGeoFireClass.sendDataToFirebaseOfUserLocation(location);
    }

    public void setGeoFireCompleteListener() {
        if (mCurrentLocationMarker != null) {
            mCurrentLocationMarker.remove();
        }
        addCurrentLocationMarker(mCurrentLocation);
        getCurrentLocationInternal(Constants.sKeyCameraZoom);
    }

    private void sendDataToFirebaseOfTaskLocation(Location location, String pTimeStamp) {
        mTaskLocation = location;
        getFirebaseInstance();
        GeoFire geoFire = mFirebaseDatabase.setTaskLocationWithGeoFire();
        if (mTaskLocation != null) {
            geoFire.setLocation(pTimeStamp,
                    new GeoLocation(mTaskLocation.getLatitude(), mTaskLocation.getLongitude()));
            mShownCardMarker.setTaskMarker(mTaskLocation);
            startGeoFencing(mTaskLocation.getLatitude(), mTaskLocation.getLongitude());
        }
    }

    private void startGeoFencing(double lat, double lng) {
        Geofence geofence = mGeoFencing.createGeofences(lat, lng);
        if (geofence != null) {
            GeofencingRequest geofencingRequest = mGeoFencing.createGeofenceRequest(geofence);
            if (geofencingRequest != null) {
                PendingResult<Status> statusPendingResult = mGeoFencing.addGeofence(geofencingRequest, mGoogleApiClient);
                statusPendingResult.setResultCallback(this);
            }
        }
    }

    public void setUpGeoFencingForTask(double pLat, double pLong) {
        if (pLat != 0.0 && pLong != 0.0) {
            if (CommonUtility.getTaskApplied(this)) {
                LatLng latLng = CommonUtility.getTaskLocation(this);
                double distance = MayoApplication.distance(pLat, pLong, latLng.latitude, latLng.longitude);
                if (distance > Constants.GeoFencing.sGeoFenceDitance) {
                    userMovedAway();
                } else {
                    startGeoFencing(latLng.latitude, latLng.longitude);
                }
            }

        }
    }

    /**
     * perform GeoQuery on every 200 m radius
     */
    private void setGeoQuery() {
        mNearByUsers.clear();
        mAllNearByUsers.clear();
        /**
         * mCurrentLocation.setLatitude(47.654829);
         * mCurrentLocation.setLongitude(-122.317918);
         *
         * Changed for testing*/
        mCurrentLocation.setLatitude(47.654829);
        mCurrentLocation.setLongitude(-122.317918);
        mGeoQuery = mGeoFireClass.setGeoQuery(mCurrentLocation);
        mGeoFireClass.setGeoQueryForTaskFetch(mCurrentLocation);

        mCardsDataModel.waitingTimeToFetchTaskArrayList(800, 1000);
    }

    public void getNearByUsers(String pKey, GeoLocation pGeoLocation) {
        UsersLocations usersLocations = new UsersLocations();
        usersLocations.setKey(pKey);
        usersLocations.setLatitude(pGeoLocation.latitude);
        usersLocations.setLongitude(pGeoLocation.longitude);
        setRealTimeUsers(usersLocations);
    }

    public void removeCardsFromViewPager() {
        mCardsDataModel.setViewPagerAdapter(mMapViewPagerAdapter);
        mCardsDataModel.removeCardListFromView();
        mTasksArray.clear();
        mTaskLocationsArray.clear();
    }

    public void setRealTimeUsers(UsersLocations usersLocations) {
        getFirebaseInstance();
        String key = usersLocations.getKey();
        if (usersLocations.getKey().contains("Optional(")) {
            key = key.substring(key.indexOf("(") + 2, key.length() - 2);
            mFirebaseDatabase.getUsersCurrentTimeFromFirebase(key, usersLocations, mGoogleMap);
        } else {
            mFirebaseDatabase.getUsersCurrentTimeFromFirebase(key, usersLocations, mGoogleMap);
        }
    }

    // TODO: 01/08/2018 called when geoQuery loaded next task #LocationUpdate
    public void getNearByTask(String pKey, GeoLocation pGeoLocation, boolean isNewCardEnter) {
        TaskLocations taskLocations = new TaskLocations();
        taskLocations.setKey(pKey);
        taskLocations.setLatitude(pGeoLocation.latitude);
        taskLocations.setLongitude(pGeoLocation.longitude);
        Task task = CommonUtility.getTaskData(this);
        if (task != null && pKey.equals(task.getTaskID()) && !task.isCompleted()) {
            return;
        }
        mTaskLocationsArray.add(taskLocations);
        if (isNewCardEnter) {
            isNewTaskEnter = true;
            getFirebaseInstance();
            mFirebaseDatabase.getTaskFromFirebase(mTaskLocationsArray.get(mTaskLocationsArray.size() - 1));
        }
    }

    // TODO: 01/08/2018 called when geoQuery ready #LocationUpdate
    public void fetchAfterNearByTask() {
        getFirebaseInstance();
        mFirebaseDatabase.clearTaskArray();
        if (mTaskLocationsArray.size() > 0) {
            for (int i = 0; i < mTaskLocationsArray.size(); i++) {
                mFirebaseDatabase.getTaskFromFirebase(mTaskLocationsArray.get(i));
            }
        }
    }

    private void getFirebaseInstance() {
        if (mFirebaseDatabase == null) {
            mFirebaseDatabase = new FirebaseDatabase(this);
        }
    }

    public void setAllUsersIntoList(UserMarker pUserMarker) {
        if (pUserMarker != null) {
            mAllNearByUsers.add(pUserMarker);
        }
    }

    public void setUsersIntoList(String pKey, UserMarker pUserMarker) {
        if (!pKey.equals(CommonUtility.getUserId(this))) {
            if (pUserMarker != null) {
                mNearByUsers.add(pUserMarker);
            }
            if (mNearByUsers.size() > 0) {
                Log.d("live_user_marker", String.valueOf(mNearByUsers.size()));
                if (mUserLiveMarker == null) {
                    mUserLiveMarker = new UserLiveMarker(this, mNearByUsers);
                    mUserLiveMarker.startTimer();
                }
            }
            if (mNearByUsers.size() <= 3) {
                if (!CommonUtility.getFakeMarkerShown(this)) {
                    showFakeMarker();
                }
            }
        }
    }

    public void setListsOfFetchingTask(Task pTask, TaskLocations pTaskLocations) {
        TaskLatLng taskLatLng = new TaskLatLng();
        taskLatLng.setTask(pTask);
        taskLatLng.setTaskLocations(pTaskLocations);
        mTasksArray.add(taskLatLng);
        if (isNewTaskEnter) {
            MapDataModel mapDataModel = mCardsDataModel.getMapModelData(taskLatLng);
            mShownCardMarker.setOtherUsersTaskMarker(mapDataModel, 4);
            int count = 0;
            if (CommonUtility.getFakeCardOne(this)) {
                count += 1;
            }
            if (CommonUtility.getFakeCardTwo(this)) {
                count += 1;
            }
            if (CommonUtility.getFakeCardThree(this)) {
                count += 1;
            }
            mMapDataModels.add(4 - count, mapDataModel);
            mShownCardMarker.setMarkerTagsOnNewTaskFetch(mMapDataModels, 5 - count);
            mCardsDataModel.setViewPagerAdapter(mMapViewPagerAdapter);
            mCardsDataModel.sortNonExpiredCardViewList(mMapDataModels);
            isNewTaskEnter = false;
        }
    }

    public void updateTaskCardFromViewPager(Task pTask, TaskLocations pTaskLocations) {
        mCardsDataModel.setListOnUpdationOfTask(pTask, mMapDataModels, pTaskLocations);
        mCardsDataModel.sortExpiredCardViewList(mMapDataModels);
        if (mMapViewPagerAdapter != null) {
            mMapViewPagerAdapter.notifyDataSetChanged();
        }
        if (mMayoApplication.gettActivityName().equals(ChatActivity_.class.getSimpleName()) &&
                mMayoApplication.isActivityVisible() && pTask.isCompleted()) {
            mMayoApplication.hideKeyboard(mMayoApplication.getActivity().getCurrentFocus());
            mMayoApplication.getActivity().finish();
            mMayoApplication.setActivity(this);
        }
    }

    private void showFakeMarker() {
        if (isLocationEnabled(this) && mCurrentLocation != null) {
            if (mCurrentLocation.getLatitude() != 0.0 && mCurrentLocation.getLongitude() != 0.0) {
                setFakeMarker(mCurrentLocation);
            }
        }
    }


    public void onGeoQueryError(DatabaseError error) {
        mMayoApplication.showToast(MapActivity.this, String.valueOf(error));
    }

    /**
     * Perfom method when current marker going outside from current circle
     * Then remove previous fake marker all and create new fake marker
     */
    public void moveMarkerOutsideFromCurrentLocation(String pkey) {
        if (pkey.equals(CommonUtility.getUserId(this))) {
            if (mFakeMarker != null) {
                mFakeMarker.stoptimertask();
            }
            //remove previous circle
            if (mCurrentLocationGroundOverlay != null) {
                mCurrentLocationGroundOverlay.remove();
            }
            if (mCurrentLocation != null) {
                drawCircle(mCurrentLocation);
                setFakeMarker(mCurrentLocation);
            }
            setGeoQuery();
        } else {
            removeUserMarker(pkey);
        }
    }

    private void removeUserMarker(String pKey) {
        HashSet<UserMarker> hashSetUserMarker = new HashSet<>(mNearByUsers);
        Log.d("live_user_marker_timer", String.valueOf(hashSetUserMarker.size()));
        for (UserMarker userMarker : hashSetUserMarker) {
            if (userMarker.getKey().equals(pKey)) {
                userMarker.getMarker().remove();
                break;
            }
        }
        mNearByUsers = hashSetUserMarker;
    }

    public void disableLocationDialogView(final int val) {
        if (mDialog == null && !isFinishing()) {
            mDialog = CommonUtility.showCustomDialog(this, R.layout.location_disable_dialog_view);
        }
        if (mDialog != null) {
            Button NotNowButton = mDialog.findViewById(R.id.notnowbutton);
            Button SettingButton = mDialog.findViewById(R.id.settingsbutton);
            NotNowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    mDialog = null;
                }
            });
            SettingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (val == Constants.PermissionDialog.AppPermissionDialog.ordinal()) {
                        CommonUtility.goToSettings(MapActivity.this);
                    } else {
                        CommonUtility.goToSettingsForGpsLocation(MapActivity.this);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View pView, int pPosition, String pMessage) {
        int Type = mMapDataModels.get(pPosition).getFakeCardPosition();
        Constants.CardType cardType = Constants.CardType.values()[Type];
        switch (cardType) {
            case POST:
                switch (pView.getId()) {
                    case R.id.messageIcon:
                        openChatMessageView(pMessage, false, pPosition);
                        break;
                    case R.id.postButton:
                        if (!pMessage.equals(Constants.sConstantEmptyString)) {
                            getFirebaseInstance();
                            String startColor = mMapDataModels.get(pPosition).getGradientColor().getStartColor().substring(1);
                            String endColor = mMapDataModels.get(pPosition).getGradientColor().getEndColor().substring(1);
                            Task task = mFirebaseDatabase.setTask(pMessage, this, startColor, endColor);
                            mFirebaseDatabase.writeNewTask(task.getTaskID(), task);
                            mFirebaseDatabase.writeNewChannelForCurrentTask(task.getTaskID());
                            mFirebaseDatabase.writeTaskCreatedInUserNode(CommonUtility.getUserId(this), task.getTaskID());
                            mFirebaseDatabase.setListenerForEnteringTask(task.getTaskID());
                            mFirebaseDatabase.getAllMessagesOfTaskEnteredByUser(task.getTaskID());
                            mFirebaseDatabase.sendPushNotificationToNearbyUsers(mAllNearByUsers, task.getTaskID());
                            CommonUtility.scheduleNotification(this);
                            TaskLatLng taskLatLng = new TaskLatLng();
                            taskLatLng.setTask(task);
                            TaskLocations taskLocations = new TaskLocations();
                            taskLocations.setLongitude(mCurrentLocation.getLongitude());
                            taskLocations.setLatitude(mCurrentLocation.getLatitude());
                            taskLocations.setKey(task.getTaskID());
                            taskLatLng.setTaskLocations(taskLocations);
                            mMapDataModels.get(pPosition).setTaskLatLng(taskLatLng);
                            sendDataToFirebaseOfTaskLocation(mCurrentLocation, task.getTaskID());
                            // user applied for task
                            CommonUtility.setTaskApplied(true, MapActivity.this);
                            //save Task location
                            CommonUtility.setTaskLocation(mCurrentLocation, MapActivity.this);
                            // save task data
                            CommonUtility.setTaskData(task, MapActivity.this);
                            mFirebaseDatabase.setUpdateTimeOfCurrentTask(task.getTaskID());
                        }
                        break;
                    case R.id.doneIcon:
                        if (mMayoApplication.isConnected()) {
                            setParentAnmationOfQuest(R.anim.slide_up);
                            setParentQuestButton(View.VISIBLE);
                        } else {
                            showInternetConnectionDialog();
                        }
                        break;
                }
                break;
            case FAKECARDTWO:
                openChatMessageView(Constants.sConstantEmptyString, false, pPosition);
                break;
            case DEFAULT:
                switch (pView.getId()) {
                    case R.id.expiryImageView:
                        openChatMessageView(pMessage, true, pPosition);
                        break;
                    case R.id.canHelped:
                        openChatMessageView(pMessage, false, pPosition);
                        break;
                }
                break;
        }
    }

    public void showInternetConnectionDialog() {
        final Dialog dialog = CommonUtility.showCustomDialog(this, R.layout.internet_connection);
        if (dialog != null) {
            dialog.findViewById(R.id.noConnectionButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    @Click(R.id.cancelButton)
    public void onClickOfCancel() {
        setParentQuestButton(View.GONE);
        setParentAnmationOfQuest(R.anim.slide_down);
    }

    @Click(R.id.questButton)
    public void onClockOfQuestButton() {
        setParentQuestButton(View.GONE);
        setParentAnmationOfQuest(R.anim.slide_down);

        final ArrayList<Message> mMessageList = mFirebaseDatabase.getLastFiveMessagesFromMessagesList();
        if (!mMessageList.isEmpty()) {
            final Dialog dialog = CommonUtility.showCustomDialog(MapActivity.this, R.layout.thanks_dialog);
            dialog.setCancelable(false);
            if (CommonUtility.getTaskApplied(MapActivity.this)) {
                Task task = CommonUtility.getTaskData(this);
                Drawable drawable = CommonUtility.getGradientDrawable("#" + task.getStartColor(), "#" + task.getEndColor(), this);
                dialog.findViewById(R.id.thanksDialogBackground).setBackground(drawable);
            }
            setThanksDialog(dialog, mMessageList);
        } else {
            onTaskExpiredAndNoOneHelped();
        }

        CommonUtility.unScheduleNotifications(this);
        CommonUtility.setTaskApplied(false, MapActivity.this);
        TaskExpiration.INSTANCE.clearExpirationJob();
        if (mGeoFencing != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGeoFencing.stopGeoFenceMonitoring(mGoogleApiClient);
        }

        removeMessageListener();
        mMapViewPagerAdapter.setPostMessageView();
    }

    private void setThanksDialog(final Dialog pDialog, final List<Message> mMessageList) {
        isThanksDialogOpen = true;
        getFirebaseInstance();
        RecyclerView mHelpRecyclerView = pDialog.findViewById(R.id.helpPersonsRecyclerView);
        final TextView mThanksTextView = pDialog.findViewById(R.id.thanks);
        TextView noThanksTextView = pDialog.findViewById(R.id.no_one_helped);
        pDialog.findViewById(R.id.lineThanks).setAlpha(Constants.sTransparencyLevelFade);

        final ArrayList<Message> mhelpMessageList = new ArrayList<>();
        ThanksChatAdapter helpChatAdapter = new ThanksChatAdapter(mMessageList, this, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, boolean isSelected) {
                if (mMessageList != null && mMessageList.size() > 0) {
                    Message message = mMessageList.get(position);
                    if (isSelected) {
                        mhelpMessageList.add(message);
                    } else {
                        for (int i = 0; i < mhelpMessageList.size(); i++) {
                            if (mhelpMessageList.get(i).getSenderId().equals(message.getSenderId())) {
                                mhelpMessageList.remove(i);
                            }
                        }
                    }
                    if (mThanksTextView != null) {
                        if (mhelpMessageList.size() > 0) {
                            mThanksTextView.setAlpha(Constants.sNonTransparencyLevel);
                        } else {
                            mThanksTextView.setAlpha(Constants.sTransparencyLevelFade);
                        }
                    }
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mHelpRecyclerView.setLayoutManager(layoutManager);
        mHelpRecyclerView.setAdapter(helpChatAdapter);
        mThanksTextView.setAlpha(Constants.sTransparencyLevelFade);
        noThanksTextView.setAlpha(Constants.sNonTransparencyLevel);
        mThanksTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mThanksTextView.getAlpha() == Constants.sNonTransparencyLevel) {
                    if (mhelpMessageList.size() > 0) {
                        Task task = CommonUtility.getTaskData(MapActivity.this);
                        updateTaskData(getString(R.string.STATUS_FOR_THANKED), task, mhelpMessageList);
                        pDialog.dismiss();
                        isThanksDialogOpen = false;
                    }
                }
            }
        });
        noThanksTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTaskExpiredAndNoOneHelped();
                pDialog.dismiss();
                isThanksDialogOpen = false;
            }
        });
    }

    private void onTaskExpiredAndNoOneHelped() {
        Task task = CommonUtility.getTaskData(MapActivity.this);
        updateTaskData(getString(R.string.STATUS_FOR_NOT_HELPED), task, new ArrayList<Message>());
    }

    private void removeMessageListener() {
        getFirebaseInstance();
        mFirebaseDatabase.removeGetAllMessagesListener();
    }

    public void updateTaskDataDueToTaskExpired(Task task, final List<Message> mMessageList) {
        if (task.isRecentActivity() && !isThanksDialogOpen && !isFinishing()) {
            if (!mMessageList.isEmpty()) {
                Dialog dialog = CommonUtility.showCustomDialog(MapActivity.this, R.layout.thanks_dialog);
                if (dialog != null) {
                    Drawable drawable = CommonUtility.getGradientDrawable("#" + task.getStartColor(), "#" + task.getEndColor(), this);
                    dialog.findViewById(R.id.thanksDialogBackground).setBackground(drawable);
                    setThanksDialog(dialog, mMessageList);
                }
            } else {
                onTaskExpiredAndNoOneHelped();
            }
        }
    }

    private void updateTaskData(String pMessage, Task task, List<Message> mhelpMessageList) {
        //task will complete when expired or done
        updateTask(true, pMessage, task.isUserMovedOutside(), mhelpMessageList);
        boolean alreadyCompleted = task.isCompleted(); // prevent double notification
        task.setCompleted(true);
        task.setTimeUpdated(CommonUtility.getLocalTime());
        task.setCompleteType(pMessage);
        CommonUtility.setTaskData(task, this);
        TaskLatLng taskLatLng = mCardsDataModel.setTaskLatlngModel(task, CommonUtility.getTaskLocation(MapActivity.this));
        MapDataModel mapDataModel = mCardsDataModel.getMapModelData(taskLatLng);
        mMapDataModels.add(mapDataModel);
        mCardsDataModel.setViewPagerAdapter(mMapViewPagerAdapter);
        mCardsDataModel.sortExpiredCardViewList(mMapDataModels);
        CommonUtility.setTaskApplied(false, this);
        if (mMapDataModels.size() > 0 && mMapDataModels.get(0).getCardLatlng() != null &&
                mMapDataModels.get(0).getCardLatlng().getMarker() != null) {
            mMapDataModels.get(0).getCardLatlng().getMarker().remove();
            mMapDataModels.get(0).getCardLatlng().setMarker(null);
        }

        if(!alreadyCompleted) {
            List<String> userIds = Utils.mapMessageListToUserIds(mhelpMessageList);
            mFirebaseDatabase.notifyParticipantsTaskExpired(task, userIds);
        }
    }

    public void showLocalNotification(int pId, String pTitle, String pBody) {
        NotificationCompat.Builder builder = CommonUtility.notificationBuilder(this, pTitle, pBody);
        Intent notificationIntent = new Intent(this, MapActivity_.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, pId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        mMayoApplication.showLocalNotification(0, builder, this);
    }

    private void setParentQuestButton(int visibility) {
        mParentQuestLayout.setVisibility(visibility);
    }

    private void setParentAnmationOfQuest(int pAnimType) {
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(), pAnimType);
        mParentQuestLayout.startAnimation(slide_up);
    }

    private void updateTask(boolean pCompleteOrNot, String pCompleteType, boolean pUserMoveOutside, List<Message> mhelpMessageList) {
        getFirebaseInstance();
        Task taskData = CommonUtility.getTaskData(MapActivity.this);
        ArrayList<String> stringArrayList = new ArrayList<>();
        if (pCompleteType.equals(getString(R.string.STATUS_FOR_THANKED))) {
            for (Message message : mhelpMessageList) {
                stringArrayList.add(message.getSenderId());
                mFirebaseDatabase.handleUsersHelpedButtonPressed(message.getSenderId(), taskData);
                mFirebaseDatabase.updatePointsAtFirebaseServer(message.getSenderId(), false);
            }
        }
        Task task = mFirebaseDatabase.updateTaskOnFirebase(
                pCompleteOrNot,
                pCompleteType,
                this,
                pUserMoveOutside,
                taskData.isRecentActivity(),
                stringArrayList);
        mFirebaseDatabase.updateTask(task.getTaskID(), task);
    }

    public void userMovedAway() {
        if (CommonUtility.getTaskApplied(this)) {
            Task task = CommonUtility.getTaskData(this);
            if (task != null) {
                if (!task.getTaskDescription().equals(Constants.sConstantEmptyString) && !task.isUserMovedOutside()) {
                    task.setUserMovedOutside(true);
                    CommonUtility.setTaskData(task, this);
                    if (task.isRecentActivity()) {
                        updateTask(false, Constants.sConstantEmptyString, true, new ArrayList<Message>());
                    } else {
                        updateTask(true, getString(R.string.STATUS_FOR_MOVING_OUT), true, new ArrayList<Message>());
                        TaskExpiration.INSTANCE.clearExpirationJob();
                        showLocalNotification(2, getString(R.string.out_of_range), getString(R.string.post_again));
                        CommonUtility.setTaskApplied(false, this);
                        if (mMapViewPagerAdapter != null) {
                            mMapViewPagerAdapter.setPostMessageView();
                        }
                    }
                }
            }
        }
    }

    public void openChatMessageView(String pMessage, boolean pExpiredCard, int pPosition) {
        if (pMessage.equals(Constants.sConstantEmptyString)) {
            ChatActivity_.intent(MapActivity.this).start();
        } else {
            ChatActivity_.intent(MapActivity.this).extra(Constants.sPostMessage, pMessage)
                    .extra(Constants.sQuestMessageShow, pExpiredCard).extra(Constants.sCardsData, mMapDataModels.get(pPosition)).start();
        }
        overridePendingTransition(R.anim.slide_out_left, R.anim.push_down_out);
    }

    public void openChatViewFromNotification(String pMessage, boolean pExpiredCard, String pTaskId) {
        MapDataModel mapDataModel = null;
        for (MapDataModel map : mMapDataModels) {
            if (map.getTaskLatLng().getTask().getTaskID().equals(pTaskId)) {
                mapDataModel = map;
                break;
            }
        }
        if (mapDataModel != null) {
            ChatActivity_.intent(MapActivity.this).extra(Constants.sPostMessage, pMessage)
                    .extra(Constants.sQuestMessageShow, pExpiredCard).
                    extra(Constants.sCardsData, mapDataModel).start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    public void setRecentActivity(boolean pVal) {
        for (int i = 0; i < mMapDataModels.size(); i++) {
            if (mMapDataModels.get(i).getFakeCardPosition() == Constants.CardType.POST.getValue()) {
                mMapDataModels.get(i).getTaskLatLng().getTask().setRecentActivity(pVal);
                break;
            }
        }
    }

    @Override
    public void taskExpireAlert() {
        if (!isFinishing()) {
            final Dialog dialog = CommonUtility.showCustomDialog(this, R.layout.quest_completed);
            if (dialog != null) {
                dialog.findViewById(R.id.questCompleted).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        }
    }

    private void registerReceiver() {
        LocalBroadcastManager lbc = LocalBroadcastManager.getInstance(this);
        mReceiver = new GoogleReceiver(this);
        lbc.registerReceiver(mReceiver, new IntentFilter(Constants.BroadCastReceiver.sBroadCastName));
    }

    class GoogleReceiver extends BroadcastReceiver {

        MapActivity mActivity;

        public GoogleReceiver(Activity activity) {
            mActivity = (MapActivity) activity;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            userMovedAway();
            if (mGeoFencing != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mGeoFencing.stopGeoFenceMonitoring(mGoogleApiClient);
            }
        }
    }

    public void setNotificationReceiverBroadcast() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MAIN);
        mBroadCastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getFirebaseInstance();
                if (intent.getStringExtra(Constants.Notifications.sChannelId) != null) {
                    String channelId = intent.getStringExtra(Constants.Notifications.sChannelId);
                    if (mMapDataModels != null && mViewPagerMap != null) {
                        for (int i = 0; i < mMapDataModels.size(); i++) {
                            if (mMapDataModels.get(i).getTaskLatLng() != null
                                    && mMapDataModels.get(i).getTaskLatLng().getTask() != null
                                    && mMapDataModels.get(i).getTaskLatLng().getTask().getTaskID().equals(channelId)) {
                                mViewPagerMap.setCurrentItem(i);
                                break;
                            }
                        }
                    }
                }
                if (intent.getStringExtra(Constants.Notifications.sAlarmManagerNotification) != null) {
                    showLocalNotification(3, getString(R.string.quest_expire_soon), getString(R.string.helped_soon));
                }

            }
        };
        this.registerReceiver(mBroadCastReceiver, intentFilter);
    }

    private void finishAnimating2(ApngDrawable mApngDrawable) {
        mRotateImageOnMapView.setVisibility(View.GONE);
        mRotateImageOnMapView.clearAnimation();
        if (mApngDrawable == null)
            return;
        if (mApngDrawable.isRunning()) {
            mApngDrawable.stop(); // Stop animation
        }
        mImageHandsViewOnMap.setVisibility(View.GONE);
    }

    public void setScoreIntoView(int pScore, boolean isThanksDialogOpen) {
        /*
         * Show animation when FakeCard Tasks completed -
         * */
        mCountButton.setText(String.valueOf(pScore));
        if (isFakeCardCompleted) {
            if (isThanksDialogOpen) {
                CommonUtility.setPoints(pScore, this);
                animateHands();
            }
        }
        /*
         * update points in Shared Preferences -
         * */
        if (CommonUtility.getPoints(this) != pScore) {
            CommonUtility.setPoints(pScore, this);
        }
    }

    private void animateHands() {
        mImageHandsViewOnMap.setVisibility(View.VISIBLE);
        final ApngDrawable mApngDrawable = ApngDrawable.getFromView(mImageHandsViewOnMap);
        mRotateImageOnMapView.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(MapActivity.this, R.anim.rotate);
        mRotateImageOnMapView.startAnimation(animation);
        if (mApngDrawable == null) {
            finishAnimating2(mApngDrawable);
            return;
        }
        mApngDrawable.setNumPlays(1);
        mApngDrawable.setShowLastFrameOnStop(true);
        mApngDrawable.setApngListener(new ApngListener() {
            @Override
            public void onAnimationEnd(ApngDrawable apngDrawable) {
                finishAnimating2(mApngDrawable);
            }
        });
        mApngDrawable.start();
    }
}
